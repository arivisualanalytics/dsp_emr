# Electronic Medical Records DSP

## Overview

This project uses the MIMIC-III dataset, which is available free of cost to users authorized through CITI and MIT.  If you have not yet completed the process to access the database, see [PhysioNet](https://physionet.org/content/mimiciii/1.4/).

This project will use EMR ICD-9 data codes from the MIMIC dataset to predict future hospital events. Specifically, it uses the ADMISSIONS.csv and DIAGNOSES_ICD.csv files, which contain diagnosis codes.  Additional codes in the MIMIC dataset include procedure and drug codes.

## Resources

This project was built from the work detailed in two papers:

Doctor AI: Predicting Clinical Events via Recurrent Neural Networks
Edward Choi, Mohammad Taha Bahadori, Andy Schuetz, Walter F. Stewart, Jimeng Sun
arXiv preprint arXiv:1511.05942

Medical Concept Representation Learning from Electronic Health
Records and its Application on Heart Failure Prediction
Edward Choi, Andy Schuetz, Walter F. Stewart, Jimeng Sun
arXiv preprint arXiv:1602.03686

And uses the codebase developed and available here:
[https://github.com/mp2893/doctorai](https://github.com/mp2893/doctorai)
(The procedure below will copy a different version of this code.)

## Tutorial

### Step 1. Create your Cloud9 environment.

In your browser, open the [AWS at Illinois](https://aws.illinois.edu) page and sign in. Once you are redirected to AWS, open the [Cloud9 creation form](https://us-east-2.console.aws.amazon.com/cloud9/home/create).

On the first page of the form, give your Cloud9 environment a name (e.g., `<your-net-id>-emr`). Enter a description if you like. Press the "Next step" button.

On the second page of the form, use default values for all sections except "Platform", which you should change to "Ubuntu Server 18.04 LTS". Press the "Next step" button.

On the third page of the form, scroll to the bottom and press the "Create environment" button. On-screen messages will report progress, and when your environment is ready, you'll see a welcome screen.

#### Using Cloud9

Inside your Cloud9 environment, you will see a panel on the left that shows your files. You can upload a file from your computer by dragging it over this panel and dropping it into the desired folder. You can download a file to your computer by right-clicking on its name and selecting "Download" from the menu that appears. Double-clicking a file will open it for editing in a tab on the right side of the screen.

You'll also see a tab near the bottom of the screen that contains a terminal with a command prompt. Commands in the following steps should be run from the prompt in a terminal tab in your Cloud9 environment.

You can resize and rearrange the panels on the screen, create new terminal tabs and file tabs, and customize your environment in other ways; see [the tour](https://docs.aws.amazon.com/console/cloud9/tutorial-tour-ide) for more information.

#### Cloud9 hibernation

Your Cloud9 environment will hibernate if idle for more than 30 minutes or if your AWS session expires. This reduces costs and keeps your Cloud9 environment secure.

Whenever your Cloud9 environment hibernates, you can resume your session by doing the following: First, you can close your old Cloud9 tab. In a new tab, sign in at [AWS at Illinois](https://aws.illinois.edu) and then open your [Cloud9 dashboard](https://us-east-2.console.aws.amazon.com/cloud9/home?region=us-east-2). Press either one of the "Open IDE" buttons on the screen. In your restarted Cloud9 environment, your files will be restored exactly as they were, but any terminal tabs will be reinitialized. You will probably want to change back to your previous working directory (for the tutorial, `cd dsp_emr`) and reactivate your Anaconda environment (`conda activate dsp_emr`).

#### Destroying a Cloud9 environment

When you no longer need your Cloud9 environment, make sure you have another copy of all of your files, and then return to [Cloud9 on AWS](https://us-east-2.console.aws.amazon.com/cloud9/home/), press the "Delete" button, and follow the on-screen prompts.

### Step 2. Clone the code repository and change your working directory.

Run the following:  
    
    git clone git@bitbucket.org:arivisualanalytics/dsp_emr.git
    cd dsp_emr

### Step 3. Expand the disk attached to your Cloud9 environment.

The command below will reserve 40 GB of storage, which will be enough for the template project and perhaps enough for your independent project, too. If you later find you need more storage, you can run this command again, replacing 40 with the total number of gigabytes you need. Note the `. ` at the beginning of the line is required!
  
    . setup/resize-disk.sh 40

### Step 4. Install the code dependencies using Anaconda.

Run the following (again, the `. ` at the beginning of the first line is required):  
  
    . setup/install-conda.sh
    conda env create -f setup/dsp-emr-environment.yml
    conda activate dsp_emr

You should now see `(dsp_emr)` appear on the left side of your command prompt. (You might notice the command output advises you to close and re-open your current shell, but as long as you see `(dsp_emr)` on the left side of your command prompt, you can ignore the message.)

**If at any point you open a new terminal in Cloud9, or if your Cloud9 environment is restarted after hibernation, you will need to run `conda activate dsp_emr` before running any of the files in the `scripts/` directory.**

### Step 5. Download MIMIC dataset files with your PhysioNet credentials.

First, confirm that you have access to MIMIC by opening [https://physionet.org/content/mimiciii/1.4/](https://physionet.org/content/mimiciii/1.4/) in your browser.
   
Look near the top-right corner of the screen, to the left of the "Search" box. If you see your username, you can skip to the next paragraph. Otherwise, if you see "Account", click it to expand the menu and select "Login". Enter your credentials. You will be redirected to a different page, showing your projects. Return to [https://physionet.org/content/mimiciii/1.4/](https://physionet.org/content/mimiciii/1.4/).
   
Scroll to the "Files" section of the page. If you see a restricted-access warning, you still need to get access to MIMIC-III or sign the data use agreement for this project. Otherwise, you can run the following in your Cloud9 environment, replacing `[username]` with your PhysioNet username and entering your PhysioNet password when prompted:  
  
    wget --user [username] --ask-password -O data/mimic/mimic-iii-clinical-database-1.4.zip \
        https://physionet.org/content/mimiciii/get-zip/1.4/

To confirm the download was successful, run the following command. It should produce `a3eb25060b7dc0843fe2235d81707552` as output.  
  
    md5sum data/mimic/mimic-iii-clinical-database-1.4.zip

### Step 6. Extract the ADMISSIONS and DIAGNOSES_ICD tables from the MIMIC-III zip file to the data directory.

Run the following:  
  
    unzip -p data/mimic/mimic-iii-clinical-database-1.4.zip mimic-iii-clinical-database-1.4/ADMISSIONS.csv.gz | gunzip > data/mimic/ADMISSIONS.csv
    unzip -p data/mimic/mimic-iii-clinical-database-1.4.zip mimic-iii-clinical-database-1.4/DIAGNOSES_ICD.csv.gz | gunzip > data/mimic/DIAGNOSES_ICD.csv

Later, while working on your independent project, you might be interested in other tables from the MIMIC database. You can produce a list of them by running the following:  
  
    unzip -l data/mimic/mimic-iii-clinical-database-1.4.zip

### Step 7. Create mappings from Clinical Classifications Software (CCS) codes to ICD-9 codes.

The diagnoses (`dxref2015.csv`) and procedures (`prref2015.csv`) mapping data are derived from [Healthcare Cost and Utilization Project data](https://www.hcup-us.ahrq.gov/toolssoftware/ccs/ccs.jsp).

Run the following:  
  
    python3 scripts/create_ccs_dict.py data/ccs/dxref2015.csv data/ccs/ 2
    python3 scripts/create_ccs_dict.py data/ccs/prref2015.csv data/ccs/ 2

Look in data/ccs/ for the newly created files, which will have the `.json` extension. Compare the contents of these files with the source `.cvs` files in the same directory. Understand how the `.json` files are structured and how their contents are related to the source `.csv` files.

### Step 8. Create training, testing, and validation inputs for DoctorAI.

The `process_mimic.py` script reads visit data, maps diagnosis codes, and partitions the patients into sets; for each set, it will create files containing the patient ids (`pids.*`), patient visit dates (`date.*`), diagnostic codes (`seqs_visit.*`) and their labels (`seqs_labels.*`).

Run the following:  
  
    python3 scripts/process_mimic.py data/mimic/ADMISSIONS.csv \
        data/mimic/DIAGNOSES_ICD.csv data/ccs/dxref2015.json data/mimic/

### Step 9. Train a DoctorAI model.

The model will take in 4894 diagnostic codes of one visit and predicts 273 CCS codes for the next visit. The training will use 10 epochs. `python3 scripts/doctor_ai.py -h` will print details about the default structure of the model.

Run the following (the `THEANO_FLAGS` portion of the command below suppresses [a warning message that's safe to ignore](https://github.com/lvapeab/nmt-keras/issues/66)):  
  
    THEANO_FLAGS='optimizer_excluding=scanOp_pushout_output' python3 \
        scripts/doctor_ai.py data/mimic/seqs_visit.train.json \
        data/mimic/seqs_visit.test.json data/mimic/seqs_visit.valid.json \
        4894 data/mimic/seqs_label.train.json data/mimic/seqs_label.test.json \
        data/mimic/seqs_label.valid.json 273 data/mimic/model_processed_data --verbose

### Step 10. Predict the top 30 CCS codes for the subsequent visits for the patients in the test set.

Run the following:  
  
    python3 scripts/test_doctor_ai.py data/mimic/model_processed_data.9.npz \
        data/mimic/seqs_visit.test.json data/mimic/seqs_label.test.json \
        [200,200] --output_file data/mimic/predictions_processed_data.test.json --verbose

### Step 11. Convert the prediction outputs into two readable files of CCS codes.

One file will contain the top 30 predicted codes (`results_processed_data.predictions.csv`) and the other will contain the actual observed CCS codes (`results_processed_data.actuals.csv`).

Run the following:  
  
    python3 scripts/translate_codes_to_text.py data/mimic/label_types.json \
        data/ccs/dxref2015_text.json \
        data/mimic/predictions_processed_data.test.json \
        data/mimic/results_processed_data.predictions.csv \
        data/mimic/results_processed_data.actuals.csv -v

## Extensions

Many extensions are possible. For example, you could modify the model to predict timing of events or to predict only a specific type of diagnosis. You could also extend it to predict drug and procedure events in addition to diagnostics.
